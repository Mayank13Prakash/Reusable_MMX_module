##--------------------------EXPLORATORY DATA ANALYSIS-------------------------------------##
##   Author : Shubham Khandelwal                                                          ##  
##   Date : 11-12-2017                                                                    ##
##   Purpose: The code is used to define a function used to perform                       ##
##            Exploratory Data Analysis                                                   ##
##   All Rights Reserved by TheMathCompany Pvt Ltd.                                       ##
##                                                                                        ##
##   Change Log:                                                                          ##
##                                                                                        ##
##                                                                                        ##
##                                                                                        ##
##                                                                                        ##
##----------------------------------------------------------------------------------------##


##------------------------------EDA Function----------------------------------------------##
##                                                                                        ##
##        Function to perform EDA on a dataset                                            ##
##        Inputs:                                                                         ##
##        1.x                - Dataset (mandatory)                                        ##
##        2.DpVr             - Name of the Dependent Variable (NA, by default)            ##
##        3.event            - Name of the event level in Dependent                       ##
##                             Variable (1, by default)                                   ##
##        4.factorlevel      - cutoff value to consider a variable as factor for          ##
##                             graphical view (35, by default)                            ##
##        5.tablefactorlevel - cutoff value to consider a variable as factor for          ##
##                             tabular form (100, by default)                             ##
##        6.cor_cutoff       - Correlation cutoff (0.7,by default)                        ##
##        7.removeVars       - Variables to be removed, passed as vector                  ##
##                                                                                        ##
##                                                                                        ##
##                                                                                        ##
##               Outputs:                                                                 ##
##                  1.*bivariate_analysis.csv- Bivariate analysis of                      ##
##                          Independent variables with Dependent variable                 ##
##                          in tabular form                                               ##
##                  2.*bivariate_analysis.pdf- Bivariate analysis of                      ##
##                          Independent variables with Dependent variable                 ##
##                          in graphical form                                             ##
##                  3.*correlation_plots.pdf- Correlation plots for                       ##
##                          selected variables                                            ##
##                  4.*correlation_for_continous_variables.csv- Correlation               ##
##                          of continous variables in tabular form                        ##
##                  5.*nestaway_univariate_analysis.pdf- Univariate analysis              ##
##                          of each variable in grahical form                             ##
##                  6.*univariate_cat_analysis.csv- Univariate analysis of                ##
##                          categorical variables in tabular form                         ##
##                  7.*univariate_cont_analysis.csv- Univariate analysis of               ##
##                          continous variables in tabular form                           ##
##                                                                                        ##
##----------------------------------------------------------------------------------------##

setwd("C:/Users/user/Desktop/MathCo/Bivariate")
source("Get_EDA.r")

#Example is given below
Get_EDA(dataset,"flag","1",factorlevel = 35,tablefactorlevel = 100,removeVars = c("var1","var2","var3"),cor_cutoff = 0.6)
