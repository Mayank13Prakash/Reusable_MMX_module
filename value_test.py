import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


sns.set_style('whitegrid')
os.chdir("./MMX Data")
demographic = pd.read_csv("demographic_data_2.csv")     # 50 x 45
inventory = pd.read_csv("inventory_data_2.csv")     # 418k
kiosk = pd.read_csv("kiosk_data_2.csv")     # 50 x 25
transaction = pd.read_csv("transaction_data_2.csv")     # 135k

# creating one master dataset
dataset = kiosk.merge(demographic, on=['mapping_id', 'City', 'State'])     # 50 x 69
os.chdir("./Cart redemption")
redemption = pd.read_excel("cart_redemption.xlsx", sheetname=0)
