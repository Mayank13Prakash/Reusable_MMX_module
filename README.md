﻿# Basic Business Ideals
1. Provide products or services according to consumer's needs
2. Availability at reasonable price
3. Convenient supply of product
4. Information about the product to consumer through their access media

# Marketing Mix
### Set of controllable variables (4P's) to influence buyer's response.

**4 P's** that constitutes Marketing Mix and are key elements of marketing are:
1. **Product**: bundle of benefits offered to consumer at a price
2. **Price**: exchange value for product or service in terms of money, i.e. amount charged 
3. **Place** (distribution of product): distribution of product at place where it is easily accessible to consumers
4. **Promotion**: informing, persuading and influencing a consumer to make choice of product to be bought

**Note**: The 4P's are inter-dependent on one another because decision in one area affects decision in another areas.
Objective of composition of the factors is to:
- Create highest level of customer satisfaction
- Meet organisational objective at the same time

## Product Mix
Products are broadly classified on basis of:
- Use
- Durability
- Tangibility

**Based on USE** products are classified as:
- **Consumer goods**: personal consumption
- **Industrial goods**: input in production of another product, for commercial purpose

    **Consumer Goods** are further classified as:
    - **Convenience Goods**: buying doesn’t involve much of decision making
    - **Shopping Goods**: lack of complete prior information in decision making
    - **Speciality Goods**: decision making involves informed product details
    
    **Industrial Goods** involves cost conscious, knowledgeable and informed decision making


**Based on DURABILITY** products are classified as:
- **Durable Goods** 
	- long lasting products
	- more personal selling efforts
	- more profit margin
	- purchase decision is also affected by seller's reputation and services provided pre-sale and after-sale 
- **Non-Durable Goods**
	- lasts for few uses
	- low price margin
	- heavy advertising

**Based on TANGIBILITY** products are classified as:
- **Tangible Goods**: exists in physical form
- **Non-Tangible Goods**: services provided to individual consumer or organisational buyers

## Price Mix
Price of a good is likely to have inverse relationship with sale of a product, i.e. higher the price, more likely lesser the sales volume.
Factors that play important role in determining the price of product:
- **Cost**: determined by adding reasonable profit margin along side production and distribution cost
- **Demand**: responsiveness of demand to changes in price, i.e. if supply is lesser than demand, then consumers might be willing to purchase the product even at higher prices
- **Competition**: marking price depending on price charged by competitor
- **Marketing Objective**: involves but not limited to
	- maximising profit
	- maximising sales
	- survival in market
- **Government Regulation**

## Place Mix
- Affects **pricing** and **promotion strategy**

### Channel of Distribution: pathway adopted to transfer the ownership of goods and its physical transfer to consumer
**Functions of Distribution Channel**
- Establishing a regular contact with the customers and provides them the necessary information relating to the goods
- Inspection of goods by the consumers at convenient points
- Facilitates the transfer of ownership as well as the delivery of goods

**Types of Distribution Channel**
- Zero Stage: Directly from Producer to Consumers
- One Stage: One middleman, i.e. Retailer
- Two Stage: Two middlemen, i.e. Wholesaler and Retailer
- Three Stage: Three middlemen, i.e. Agent, Wholesaler, Retailer

**Factors affecting Distribution Channel**
- Nature of Market: eg. when number of buyers concentrated to few locations
- Nature of Product
- Nature of Company
- Middleman consideration: experience of individual

## Promotion Mix
- Main objective is to seek potential customers attention towards product with view to
	- arouse interest in the product
	- inform about availability
	- how the product is different from other

- Elements of promotion mix
	- **Advertising**: paid form of non-personal communication
	- **Publicity**: non-paid process of generating wide range of communication
	- **Personal Selling**: direct presentation of the product to the consumers or prospective buyers.	
	- **Sales Promotion**: short-term and temporary incentives to purchase or induce trials of new goods

# Marketing Mix Model
- Deals with optimal allocation of marketing funds
- Quantifies the impact of marketing decision on sales

Sales is decomposed into following components:
- **Base**: indicates consumer preferences (long-term)
- **Incremental**: short term variations due to promotion, price changes etc.

Commonly used Modeling Approach:
1. **Regression Model**
	- Commonly used models include
		- Linear Regression
		- log-Linear model: logarithm of function results in linear combination of parameters of the model
	- Involves predicting of unknown variable that depends on one or more variable
	- Quantifies the relative returns of different marketing mix variables
	- Facilitates
		- **what-if-analysis** to predict the impact of various marketing variables on revenues
		-  form an **optimum mix** by determining the best budget allocation
	- Maximizes total revenue

	**NOTE**: Regression equation can be linear (simple relationship) or non-linear (complex relationship)

2. **Influence Maximization Model**
	- Identifying set of nodes that influence the network the most
	- Allocating budgets to channels that have maximum influence

3. **Agent Based Model**
	- Analyses complex marketing pattern by simulating actions of producer, consumer
	- Set of rules of behaviour are developed based on behaviour of individual agent
	- Useful in designing **Social Marketing Campaign**

	**NOTE**: Difficult to fit and are subjects to individual's judgement

4. **Empirical Methods**
	- Heuristics to determine resource allocations
	- Involves identifying the factors that are important in deciding the optimum marketing mix and assigning weight to each of them by their importance

	**NOTE**: Subject to individual manager’s judgment and cannot be applied universally in all the situations or by all the organizations


### Challenges faced in Marketing Mix Model
- Incomplete Data
- Cost of building and implementing analysis process
- Usage of obtained result

# Outcome
The various outcomes from Marketing Mix Modeling are:
1. **Volume-due-to** factor: Key questions answered:
	- Volume driver and its impact on sales volume
	- Main competitor and element of their product impacting the business
	- Focus to counter competitor's action

2. **Marketing Elasticity**: involves leverage to pull and its impact. Measures include:
	- Distribution
	- Advertisement
	- Promotion

3. **Return on Investment**: plays critical role in understanding promotion investment
	- Helps in prioritizing investment across brands and across different marketing channels

4. **Portfolio Optimization**: provides toolkit to make changes in investment plan based on portfolio 

