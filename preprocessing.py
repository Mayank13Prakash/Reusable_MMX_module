import os
import dateutil.parser
import pandas as pd

final_data = pd.read_excel("final_dataset.xlsx")
os.chdir(".\MMX Data\SMS & Push Data\Push")
android_push = pd.read_csv('Android_push.csv')
del android_push['Unnamed: 1']
del android_push['Year-Month']

reorder_format = []
for i in android_push['date']:
    reorder_format.append(dateutil.parser.parse(i))

reorder_format = pd.DataFrame(reorder_format)
android_push['date'] = reorder_format
android_push.to_excel("Android.xlsx")

ios_push = pd.read_csv('IOS_push.csv')

reorder_format = []

for i in ios_push['date']:
    reorder_format.append(dateutil.parser.parse(i))

reorder_format = pd.DataFrame(reorder_format)
ios_push['date'] = reorder_format
ios_push.to_excel("iOS.xlsx")

os.chdir("../../..")
android = pd.read_excel("final_dataset.xlsx", sheetname=3)
ios = pd.read_excel("final_dataset.xlsx", sheetname=2)
push = android.merge(ios, on='Week', how='outer')
push.to_excel("push.xlsx")

os.chdir("./Promo Data")
push_promo = pd.read_excel("all_promo.xlsx", sheetname=0)
email_promo = pd.read_excel("all_promo.xlsx", sheetname=1)
channel_promo = pd.read_excel("all_promo.xlsx", sheetname=2)
all_promo = push_promo.merge(email_promo, on="Week", how = "outer")
all_promo = all_promo.merge(channel_promo, on="Week", how="outer")
redemption = pd.read_excel("Promotion by Offer Amount.xlsx", sheetname=1)

promo = all_promo.merge(redemption, on="Week", how="outer")
promo.to_excel("promo.xlsx")

promo_sends = pd.read_excel("Promo Sends and Transaction Combined.xlsx", sheet_name="promo_sends")
transaction_sends = pd.read_excel("Promo Sends and Transaction Combined.xlsx", sheetname="transaction_sends")
all_sends = transaction_sends.merge(promo_sends, on="Week", how='outer')
all_sends.to_excel("promo_transaction.xlsx")

# input merge
_push = pd.read_excel("final_dataset.xlsx", sheetname="push")
_promo =  pd.read_excel("final_dataset.xlsx", sheetname="promo")
sends_transactions = pd.read_excel("final_dataset.xlsx", sheetname="sends_transactions")

data_input = sends_transactions.merge(_promo, on='Week', how='outer')
data_input = data_input.merge(_push, on='Week', how='outer')
data_input.to_excel("input.xlsx")
